# KA_1_React

React.JS basics

Основные материалы:
https://ru.react.js.org/docs/hello-world.html
https://ru.react.js.org/docs/introducing-jsx.html
https://ru.react.js.org/docs/rendering-elements.html
https://ru.reactjs.org/docs/create-a-new-react-app.html#create-react-app
https://ru.reactjs.org/docs/handling-events.html
https://ru.reactjs.org/docs/conditional-rendering.html

Дополнительные материалы:
https://ru.reactjs.org/docs/jsx-in-depth.html
https://ru.reactjs.org/docs/state-and-lifecycle.html
https://medium.com/@divermak/%D0%B2%D1%81%D0%B5-%D1%84%D1%83%D0%BD%D0%B4%D0%B0%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D0%BF%D1%80%D0%B8%D0%BD%D1%86%D0%B8%D0%BF%D1%8B-react-js-%D1%81%D0%BE%D0%B1%D1%80%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5-%D0%B2-%D0%BE%D0%B4%D0%BD%D0%BE%D0%B9-%D1%81%D1%82%D0%B0%D1%82%D1%8C%D0%B5-ec6a97bfd1bf
Установить плагин - https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=ru
https://create-react-app.dev/docs/getting-started/

Видео:
https://learn.javascript.ru/screencast/react - первые 6 видео