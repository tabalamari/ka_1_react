import React from 'react';
// import Hello from './components/Hello'
// import ToggleTheme from './components/ToggleTheme'

// const App = () =>
//     <div>
//         <Hello user={
//             {
//                 firstName: 'Mari',
//                 lastName: 'Tabala'
//             }
//         }
//             showLastName />
//         <ToggleTheme />
//     </div>


class App extends React.Component {
constructor(props) {
    super(props);

    this.state={ temperature: 18 };

    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
}

    render() {

        const currentStyle = {
            color: this.state.temperature < 11 ? 'blue' : this.state.temperature > 25 ? 'red' : 'green'
        }
        return (
            <div>
                <button onClick={ this.decrement }>-</button>
                 Current temperature: <span style = {currentStyle}>{this.state.temperature}</span> 
                 {this.gitStatus()}
                <button onClick={ this.increment }>+</button>
            </div>
        )
    }
    increment(){
        if(this.state.temperature < 35) {
            this.setState({ temperature: this.state.temperature +1 })
        }
    }
    decrement(){
        if(this.state.temperature <= 0) return;
        this.setState({ temperature: this.state.temperature -1 })
    
    }

    gitStatus(){
        if(this.state.temperature < 11) return 'Cold';
        if(this.state.temperature > 25) return 'Hot';
    }
}

export default App;