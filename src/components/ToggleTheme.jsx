import React, { Component } from 'react';
import '../style/theme.css'

class ToggleTheme extends Component {
    // state = {
    //     isDark: false
    // }

    constructor() {
        super();
        this.state = {
            isDark: true
        }
        this.handleToggleClick = this.handleToggleClick.bind(this);
    }

    handleToggleClick() {
        this.setState({
            isDark: !this.state.isDark
        })
    }


    render() {
        const themeClassName = this.state.isDark ? 'dark' : '';
        const styles = {padding: '20px'}
        return (
            <div>
                <span> {this.state.isDark ? 'Dark ' : 'Light '} </span>
                <button className = {themeClassName} style={styles} 
                // style ={{padding: '30px'}} 
                onClick={this.handleToggleClick.bind(this)}>Toggle theme</button>
            </div>)
    }
}
export default ToggleTheme;