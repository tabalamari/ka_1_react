import React, { Component } from 'react';

// Через функціональний компонент:

// function Hello(props) {
//     const {
//         user,
//         showLastName
//     } = props
//     return (
// <div>
//     <h1>Hi {user.firstName} {showLastName && user.lastName}
//     </h1>
// </div>);
// }


// Через функціональний компонент, функція-стрілка:

// const Hello = ({ user, showLastName }) => (
//     <div>
//         <h1>Hi {user.firstName} {showLastName && user.lastName}
//         </h1>
//     </div>);


//Через классовий компонент:

class Hello extends Component {
    render() {
        console.log('It is props: ', this.props);

        const {
            user,
            showLastName
        } = this.props
        return (
            <div>
                <h1>Hi {user.firstName} {showLastName && user.lastName}
                </h1>
            </div>);
    }
}

export default Hello;